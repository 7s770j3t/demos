﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class RuleModel
    {
        public static string table = @"{
    ""name"": ""CREATED FROM C#"",
    ""description"": ""HELLO FROM C#"",
    ""inputSchema"": {
        ""Input attribute"": {}
    },
    ""outputSchema"": {
        ""Output Attribute"": {}
    },
    ""decisionTable"": {
        ""columns"": [
            {
                ""condition"": {
                    ""type"": ""simple"",
                    ""inputVariable"": ""Input attribute"",
                    ""name"": ""New Condition""
                },
                ""columnId"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
                ""type"": ""input""
            },
            {
                ""columnOutput"": {
                    ""type"": ""simple"",
                    ""outputVariable"": ""Output Attribute"",
                    ""name"": ""New Result""
                },
                ""columnId"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
                ""type"": ""output""
            }
        ],
        ""rows"": [
            {
                ""cells"": [
                    {
                        ""column"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
                        ""scalarCondition"": {
                            ""value"": """",
                            ""operator"": ""anything""
                        },
                        ""type"": ""input""
                    },
                    {
                        ""column"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
                        ""outputScalarValue"": {
                            ""type"": ""common"",
                            ""value"": ""Hello from Solver""
                        },
                        ""type"": ""output""
                    }
                ]
            }
        ]
    },
    ""type"": ""decision-table"",
    ""status"": ""published"",
    ""tags"": [],
    ""createdIn"": ""2022-05-05T10:51:52.015Z"",
    ""lastUpdate"": ""2022-05-05T10:51:52.015Z""
}";
    }
}