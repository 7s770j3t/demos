﻿using ConsoleApp1;
using DecisionRules;
using Newtonsoft.Json;
using System.Net;

public class Test
{
    static readonly CustomDomain domain = new("test.api.decisionrules.io", Enums.Protocol.HTTPS);
    
    static readonly string solverApikey = "Zp56inLN9cu5UOwzCsqE0U781qT0dYUZbh6-l7JG0HCZv-loO-WLRY2ZtvYjXeFV";
    static readonly string managementApiKey = "nrUyc-iFkfHq1XKnVlOqWUfyFqxGm7raXkMMFphOOkHEZRpQy3lsJUFh2xJVrtUW";

    static readonly Solver rule = new(Test.solverApikey, domain, );
    static readonly Management management = new(Test.managementApiKey, Test.domain);

    static readonly string ruleId = "8877841d-aa60-e51e-0112-dbdd1964aafc";
    static readonly string solverRule = "e69ac584-27c6-e3e9-a38a-cf2b72d2654a";
    static readonly string solverRuleFlow = "f74d4ad5-2698-32cd-eca0-9b6319e82166";
    static readonly string ruleFlowId = "0d1ecedf-1fdc-c8e3-59e6-bbae20219c1b";
    static readonly string spaceId = "e0eb3805-b887-9cf8-7c21-d664df5d0eb0";

    public async static Task SolverTest()
    {
        Console.WriteLine("====SOLVER TEST==== \n");

        TestRuleRequest myData = new() { Say = "Decision table c#"};
        TestRuleRequest myData2 = new() { Say = "Rule flow c#" };

        try
        {
            Console.WriteLine("==DECISION TABLE SOLVE TEST== \n");

            List<TestRuleRequest> bulkData = new List<TestRuleRequest>();

            bulkData.Add(myData);
            bulkData.Add(myData);
            bulkData.Add(myData);

            var solvedRule = await Test.rule.SolveRule<List<TestRuleRequest>, Object>(solverRule, bulkData,);

            var data = solvedRule;

            Console.WriteLine("RESULT: \n");
            Console.WriteLine(data);

            Console.WriteLine("\n==RULE FLOW SOLVE TEST== \n");
            var solvedRuleFlow = await Test.rule.SolveRuleFlow<TestRuleRequest, Object>(solverRuleFlow, myData2);

            var data2 = solvedRuleFlow;

            Console.WriteLine("RESULT: \n");
            Console.WriteLine(data2);


        } catch(Exception e) {
            Console.WriteLine(e);
        }
        Console.WriteLine("\n====END OF SOLVER TEST====\n");
    }
    public static void RuleCrudTest()
    {
        Console.WriteLine("====CRUD RULE TEST==== \n");
        try
        {
            Console.WriteLine("====GET RULE TEST==== \n");
            var getRule = Test.management.GetRule<Object>(ruleId).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(getRule);

            /*
            Console.WriteLine("\n====GET SPACE TEST==== \n");
            var getSpace = Test.management.GetSpace<Object>().Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(getSpace);
            */

            Console.WriteLine("====CREATE RULE TEST====");
            var data = JsonConvert.DeserializeObject<Object>(RuleModel.table);
            var createRule = Test.management.CreateRule<Object, Object>(spaceId, data).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(createRule);

            Console.WriteLine("====UPDATE RULE TEST====");
            HttpStatusCode updateRule = Test.management.UpdateRule<Object>(ruleId, data, 1).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(updateRule);

            Console.WriteLine("====DELETE RULE TEST====");
            HttpStatusCode deleteRule = Test.management.DeleteRule("c0700660-8d93-ae4b-118c-6aee972d6bcb", 1).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(deleteRule);


        } catch(Exception e)
        {
            Console.WriteLine(e);
        }
        Console.WriteLine("\n====END OF RULE CRUD TEST====\n");
    }

    public static void RuleFlowCrudTest() 
    {
        Console.WriteLine("====CRUD RULEFFLOW TEST==== \n");
        try
        {
            Console.WriteLine("====GET RULEFFLOW TEST==== \n");
            var getRuleFlow = Test.management.GetRuleFlow<Object>(ruleFlowId).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(getRuleFlow);

            
            Console.WriteLine("====CREATE RULEFFLOW TEST====");
            
            var createRule = Test.management.CreateRuleFlow<Object, Object>(getRuleFlow).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(createRule);
            

            Console.WriteLine("====UPDATE RULEFFLOW TEST====");
            HttpStatusCode updateRule = Test.management.UpdateRuleFlow<Object>(ruleFlowId, getRuleFlow, 1).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(updateRule);

            Console.WriteLine("====EXPORT RULEFFLOW TEST====");
            var exportRuleFlow = Test.management.ExportRuleFlow<Object>(ruleFlowId).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(exportRuleFlow);

            List<Object> requestForImport = new List<Object>();
            requestForImport.Add(getRuleFlow);

            Console.WriteLine("====IMPORT RULEFFLOW TEST====");
            var importRuleFlow1 = Test.management.ImportRuleFlow<List<Object>, Object>(requestForImport).Result;

            Console.WriteLine("RESULT 1\n");
            Console.WriteLine(importRuleFlow1);
          
            var importRuleFlow2 = Test.management.ImportRuleFlow<List<Object>, Object>(requestForImport, "466738fa-ad6b-c68d-0e7b-93ec8da2b5c1").Result;

            Console.WriteLine("RESULT 2\n");
            Console.WriteLine(importRuleFlow2);

            var importRuleFlow3 = Test.management.ImportRuleFlow<List<Object>, Object>(requestForImport, "466738fa-ad6b-c68d-0e7b-93ec8da2b5c1", 2).Result;

            Console.WriteLine("RESULT 2\n");
            Console.WriteLine(importRuleFlow2);

            Console.WriteLine("====DELETE RULEFFLOW TEST====");
            HttpStatusCode deleteRule = Test.management.DeleteRule("ccfca657-23b9-09d1-37af-1e00555227b9", 1).Result;

            Console.WriteLine("RESULT \n");
            Console.WriteLine(deleteRule);

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        Console.WriteLine("\n====END OF CRUD RULEFLOW TEST====\n");
    }

    public static void TagTest()
    {
        string[] tags = new string[]{"DOTNET"};
        string[] tagsToDelete = new string[] { "FROMSDK" };
        var getTags = Test.management.GetSpaceItems<Object>(tags).Result;

        Console.WriteLine(getTags);

        string tag = @"[{""tagName"":""FROMSKD"",""color"":""default""}]";

        Object tagsObject = JsonConvert.DeserializeObject<Object>(tag);

        var addTag = Test.management.UpdateTags<Object, Object>(tagsObject, "e69ac584-27c6-e3e9-a38a-cf2b72d2654a").Result;

        Console.WriteLine(addTag);

        var deleteTag = Test.management.DeleteTags("e69ac584-27c6-e3e9-a38a-cf2b72d2654a", tagsToDelete).Result;

        Console.WriteLine(deleteTag);
    }

    public static void StatusTest()
    {
        var changeStatusRule = Test.management.ChangeRuleStatus<Object>("e69ac584-27c6-e3e9-a38a-cf2b72d2654a", "pending", 1).Result;
        Console.WriteLine(changeStatusRule);
        var changeStatusRuleFlow = Test.management.ChangeRuleFlowStatus<Object>("c175e171-f675-5e02-b37d-808c6d58ea15", "pending", 1).Result;
        Console.WriteLine(changeStatusRuleFlow);
    }

    public async static Task Main()
    {
        await Test.SolverTest();
        //Test.RuleCrudTest();
        //Test.RuleFlowCrudTest();
        //Test.TagTest();
        //Test.StatusTest();
    }
}

internal class TestRuleRequest
{
    public string? Say { get; set; }
}

internal class TestRuleResponse
{
    public string? Parrot { get; set; }
}