﻿//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using DecisionTableMock;
//
//    var temperatures = Temperatures.FromJson(jsonString);

namespace DecisionTableMockClass
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Temperatures
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("inputSchema")]
        public InputSchema InputSchema { get; set; }

        [JsonProperty("outputSchema")]
        public OutputSchema OutputSchema { get; set; }

        [JsonProperty("decisionTable")]
        public DecisionTable DecisionTable { get; set; }
    }

    public partial class DecisionTable
    {
        [JsonProperty("columns")]
        public List<Column> Columns { get; set; }

        [JsonProperty("rows")]
        public List<Row> Rows { get; set; }
    }

    public partial class Column
    {
        [JsonProperty("condition", NullValueHandling = NullValueHandling.Ignore)]
        public Condition Condition { get; set; }

        [JsonProperty("columnId")]
        public Guid ColumnId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("validValues", NullValueHandling = NullValueHandling.Ignore)]
        public List<ValidValue> ValidValues { get; set; }

        [JsonProperty("columnOutput", NullValueHandling = NullValueHandling.Ignore)]
        public ColumnOutput ColumnOutput { get; set; }
    }

    public partial class ColumnOutput
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("outputVariable")]
        public string OutputVariable { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Condition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("inputVariable")]
        public string InputVariable { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class ValidValue
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public Value Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("display", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Display { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? ValueValue { get; set; }

        [JsonProperty("date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Date { get; set; }

        [JsonProperty("time", NullValueHandling = NullValueHandling.Ignore)]
        public string Time { get; set; }

        [JsonProperty("timezone", NullValueHandling = NullValueHandling.Ignore)]
        public string Timezone { get; set; }
    }

    public partial class Row
    {
        [JsonProperty("cells")]
        public List<Cell> Cells { get; set; }
    }

    public partial class Cell
    {
        [JsonProperty("column")]
        public Guid Column { get; set; }

        [JsonProperty("scalarCondition", NullValueHandling = NullValueHandling.Ignore)]
        public ScalarCondition ScalarCondition { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("outputScalarValue", NullValueHandling = NullValueHandling.Ignore)]
        public OutputScalarValue OutputScalarValue { get; set; }
    }

    public partial class OutputScalarValue
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class ScalarCondition
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("operator")]
        public string Operator { get; set; }
    }

    public partial class InputSchema
    {
        [JsonProperty("Input attribute")]
        public PutAttribute InputAttribute { get; set; }
    }

    public partial class PutAttribute
    {
    }

    public partial class OutputSchema
    {
        [JsonProperty("Output Attribute")]
        public PutAttribute OutputAttribute { get; set; }
    }

    public partial class Temperatures
    {
        public static Temperatures FromJson(string json) => JsonConvert.DeserializeObject<Temperatures>(json, DecisionTableMockClass.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Temperatures self) => JsonConvert.SerializeObject(self, DecisionTableMockClass.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
