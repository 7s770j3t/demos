﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecisionRulesUnitTests.MockData
{
    internal class RuleFlowMockJson
    {
        public static string RuleFlowMock = @"{
  ""name"": ""CREATED FROM C#"",
  ""description"": ""This is sample description"",
  ""type"": ""composition"",
  ""status"": ""published"",
  ""inputSchema"": {
    ""Input attribute"": {}
  },
  ""outputSchema"": {
    ""Output Attribute"": {}
  },
  ""visualEditorData"": {
    ""drawflow"": {
      ""Home"": {
        ""data"": {}
      }
    }
  },
  ""dataTree"": {
    ""children"": [
      {
        ""baseId"": ""null""
      },
      {
        ""children"": [
          {
            ""children"": [
              null
            ],
            ""globalVariable"": ""end"",
            ""mapping"": [
              {
                ""key"": ""finalPrice"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""prices.finalPrice""
              },
              {
    ""key"": ""crudePrice"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""prices.crudePrice""
              },
              {
    ""key"": ""message"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""message""
              }
            ]
          }
        ]
      },
      {
    ""globalVariable"": ""Rule_1""
      },
      {
    ""mapping"": [
          {
        ""key"": ""period"",
            ""source"": ""start"",
            ""sourceVariable"": ""period""
          },
          {
        ""key"": ""productType"",
            ""source"": ""start"",
            ""sourceVariable"": ""productType""
          },
          {
        ""key"": ""promoCode"",
            ""source"": ""start"",
            ""sourceVariable"": ""promoCode""
          }
        ]
      }
    ],
    ""globalVariable"": ""start"",
    ""mapping"": []
  }
}";

        public static string RuleFlowUpdateMock = @"{
  ""name"": ""UPDATED FROM C#"",
  ""description"": ""This is sample description"",
  ""type"": ""composition"",
  ""status"": ""published"",
  ""inputSchema"": {
    ""Input attribute"": {}
  },
  ""outputSchema"": {
    ""Output Attribute"": {}
  },
  ""visualEditorData"": {
    ""drawflow"": {
      ""Home"": {
        ""data"": {}
      }
    }
  },
  ""dataTree"": {
    ""children"": [
      {
        ""baseId"": ""null""
      },
      {
        ""children"": [
          {
            ""children"": [
              null
            ],
            ""globalVariable"": ""end"",
            ""mapping"": [
              {
                ""key"": ""finalPrice"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""prices.finalPrice""
              },
              {
    ""key"": ""crudePrice"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""prices.crudePrice""
              },
              {
    ""key"": ""message"",
                ""source"": ""Rule_1"",
                ""sourceVariable"": ""message""
              }
            ]
          }
        ]
      },
      {
    ""globalVariable"": ""Rule_1""
      },
      {
    ""mapping"": [
          {
        ""key"": ""period"",
            ""source"": ""start"",
            ""sourceVariable"": ""period""
          },
          {
        ""key"": ""productType"",
            ""source"": ""start"",
            ""sourceVariable"": ""productType""
          },
          {
        ""key"": ""promoCode"",
            ""source"": ""start"",
            ""sourceVariable"": ""promoCode""
          }
        ]
      }
    ],
    ""globalVariable"": ""start"",
    ""mapping"": []
  }
}";
    }
}
