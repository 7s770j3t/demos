﻿using System;

namespace DecisionRulesUnitTests.MockData
{
    public class DecisionTableMockJson
    {
        public static string DecisionTableMock = @"{
  ""name"": ""CREATED FROM C#"",
  ""description"": ""Sample client profitability rule"",
  ""type"": ""decision-table"",
  ""status"": ""published"",
  ""inputSchema"": {
    ""Input attribute"": {}
  },
  ""outputSchema"": {
    ""Output Attribute"": {}
  },
  ""decisionTable"": {
    ""columns"": [
      {
        ""condition"": {
          ""type"": ""simple"",
          ""inputVariable"": ""Input attribute"",
          ""name"": ""New Condition""
        },
        ""columnId"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
        ""type"": ""input"",
        ""validValues"": [
          {
            ""type"": ""GENERAL"",
            ""value"": {
              ""display"": ""12"",
              ""value"": ""12""
            }
          },
          {
    ""type"": ""DATE"",
            ""value"": {
        ""date"": ""2021-09-13"",
              ""time"": ""15:52"",
              ""timezone"": ""+02:00""
            }
}
        ]
      },
      {
    ""columnOutput"": {
        ""type"": ""simple"",
          ""outputVariable"": ""Output Attribute"",
          ""name"": ""New Result""
        },
        ""columnId"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
        ""type"": ""output""
      }
    ],
    ""rows"": [
      {
        ""cells"": [
          {
            ""column"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
            ""scalarCondition"": {
              ""value"": """",
              ""operator"": ""anything""
            },
            ""type"": ""input""
          },
          {
    ""column"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
            ""outputScalarValue"": {
        ""value"": ""Hello from Solver"",
              ""type"": ""common""
            },
            ""type"": ""output""
          }
        ]
      }
    ]
  }
}";


        public static string DecisionTableUpdateMock = @"{
  ""name"": ""UPDATED FROM C#"",
  ""description"": ""Sample client profitability rule"",
  ""type"": ""decision-table"",
  ""status"": ""published"",
  ""inputSchema"": {
    ""Input attribute"": {}
  },
  ""outputSchema"": {
    ""Output Attribute"": {}
  },
  ""decisionTable"": {
    ""columns"": [
      {
        ""condition"": {
          ""type"": ""simple"",
          ""inputVariable"": ""Input attribute"",
          ""name"": ""New Condition""
        },
        ""columnId"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
        ""type"": ""input"",
        ""validValues"": [
          {
            ""type"": ""GENERAL"",
            ""value"": {
              ""display"": ""12"",
              ""value"": ""12""
            }
          },
          {
    ""type"": ""DATE"",
            ""value"": {
        ""date"": ""2021-09-13"",
              ""time"": ""15:52"",
              ""timezone"": ""+02:00""
            }
}
        ]
      },
      {
    ""columnOutput"": {
        ""type"": ""simple"",
          ""outputVariable"": ""Output Attribute"",
          ""name"": ""New Result""
        },
        ""columnId"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
        ""type"": ""output""
      }
    ],
    ""rows"": [
      {
        ""cells"": [
          {
            ""column"": ""ec57bb7c-8e90-4aee-da49-17b607a6b09a"",
            ""scalarCondition"": {
              ""value"": """",
              ""operator"": ""anything""
            },
            ""type"": ""input""
          },
          {
    ""column"": ""2e46eb73-de05-51bc-5913-4b261bbe2069"",
            ""outputScalarValue"": {
        ""value"": ""Hello from Solver"",
              ""type"": ""common""
            },
            ""type"": ""output""
          }
        ]
      }
    ]
  }
}";
    }
}
