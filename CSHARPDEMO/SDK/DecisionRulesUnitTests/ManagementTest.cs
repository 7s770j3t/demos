﻿using DecisionRules;
using NUnit.Framework;
using System.Diagnostics;
using System.Threading.Tasks;
using DecisionRulesUnitTests.MockData;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DecisionRulesUnitTests
{
    [TestFixture]
    public class ManagementTest
    {
        private static readonly string ManagementKeyP = "dvW5jxHjjcb9_8LCf8D3pA1AE1IxMFylX2tHSJnSoJDZFy29RfrE51kfCihuQCom";
        private static readonly string ManagementKeyT = "GMfielBTjgHKyTZ24EBWQ0uslMtWD0d3tntEih4P49s5weu6ezmgA7hSEepC8nuJ";

        private static readonly CustomDomain Domain = new CustomDomain("test.api.decisionrules.io", Enums.Protocol.HTTPS, 443);

        private static readonly Management Mp = new(ManagementTest.ManagementKeyP);
        private static readonly Management Mt = new(ManagementTest.ManagementKeyT, ManagementTest.Domain);

        private static string CreatedRule = "";
        private static int CreatedRuleVersion = 0;

        private static string CreatedRuleT = "";
        private static int CreatedRuleVersionT = 0;

        private static string CreatedRuleFlow = "";
        private static int CreatedRuleFlowVersion = 0;

        private static string CreatedRuleFlowT = "";
        private static int CreatedRuleFlowVersionT = 0;

        [Test]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", 0)]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", 1)]
        public async Task GetRuleTest(string ruleId, int version)
        {
            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.GetRule<object>(ruleId, version);
            }
            else
            {
                result = await ManagementTest.Mp.GetRule<object>(ruleId);
            }

            string value = result.ruleId;

            Assert.AreEqual(value, ruleId);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("f2b91506-d607-cd45-8df4-f29f5dacaa84", 0)]
        [TestCase("f2b91506-d607-cd45-8df4-f29f5dacaa84", 1)]
        public async Task GetRuleTestWithCustomDomain(string ruleId, int version)
        {
            Management m = new(ManagementTest.ManagementKeyT, ManagementTest.Domain);

            dynamic result;

            if (version > 0)
            {
                result = await m.GetRule<object>(ruleId, version);
            }
            else
            {
                result = await m.GetRule<object>(ruleId);
            }

            string value = result.ruleId;

            Assert.AreEqual(value, ruleId);

            Assert.IsNotNull(result);
        }

        [Test]
        public async Task GetSpaceItemsTest()
        {
            object result = await ManagementTest.Mp.GetSpaceItems<object>();

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", 0)]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", 1)]
        public async Task GetRuleFlowTest(string itemId, int version)
        {
            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.GetRuleFlow<object>(itemId, version);
            }
            else
            {
                result = await ManagementTest.Mp.GetRuleFlow<object>(itemId);
            }

            string value = result.compositionId;

            Assert.AreEqual(value, itemId);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", 0)]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", 1)]
        public async Task GetRuleFlowTestWithCustomDomain(string itemId, int version)
        {
            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.GetRuleFlow<object>(itemId, version);
            }
            else
            {
                result = await ManagementTest.Mt.GetRuleFlow<object>(itemId);
            }

            string value = result.compositionId;

            Assert.AreEqual(value, itemId);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", 0)]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", 1)]
        public async Task ExportRuleFlowTest(string itemId, int version)
        {
            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.ExportRuleFlow<object>(itemId, version);
            }
            else
            {
                result = await ManagementTest.Mp.ExportRuleFlow<object>(itemId);
            }

            string value = result[0].compositionId;

            Assert.AreEqual(value, itemId);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", 0)]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", 1)]
        public async Task ExportRuleFlowTestWithCustomDomain(string itemId, int version)
        {
            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.ExportRuleFlow<object>(itemId, version);
            }
            else
            {
                result = await ManagementTest.Mt.ExportRuleFlow<object>(itemId);
            }

            string value = result[0].compositionId;

            Assert.AreEqual(value, itemId);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", "pending", 2)]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", "published", 2)]
        public async Task ChangeRuleStatusTest(string ruleId, string status, int version)
        {
            dynamic result = await ManagementTest.Mp.ChangeRuleStatus<dynamic>(ruleId, status, version);

            string value = result.status;

            Assert.AreEqual(value, status);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("bdb22823-fe84-aca1-da38-6f4b9f8c106a", "pending", 2)]
        [TestCase("bdb22823-fe84-aca1-da38-6f4b9f8c106a", "published", 2)]
        public async Task ChangeRuleStatusTestWithCustomDomain(string ruleId, string status, int version)
        {
            dynamic result = await ManagementTest.Mt.ChangeRuleStatus<dynamic>(ruleId, status, version);

            string value = result.status;

            Assert.AreEqual(value, status);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", "pending", 2)]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753", "published", 2)]
        public async Task ChangeRuleFlowStatusTest(string ruleId, string status, int version)
        {
            dynamic result = await ManagementTest.Mp.ChangeRuleFlowStatus<dynamic>(ruleId, status, version);

            string value = result.status;

            Assert.AreEqual(value, status);

            Assert.IsNotNull(result);
        }

        [Test]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", "pending", 2)]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240", "published", 2)]
        public async Task ChangeRuleFlowStatusTestWithCustomDomain(string ruleId, string status, int version)
        {
            dynamic result = await ManagementTest.Mt.ChangeRuleFlowStatus<dynamic>(ruleId, status, version);

            string value = result.status;

            Assert.AreEqual(value, status);
            Assert.IsNotNull(result);
        }

        [Test, Order(100)]
        public async Task CreateRule()
        {
            object requestBody = DecisionTableMockClass.Temperatures.FromJson(DecisionTableMockJson.DecisionTableMock);

            dynamic result = await ManagementTest.Mp.CreateRule<object, dynamic>(requestBody);

            ManagementTest.CreatedRule = result.ruleId;
            ManagementTest.CreatedRuleVersion = result.version;

            Assert.IsNotNull(result);
        }

        [Test, Order(100)]
        public async Task CreateRuleWithCustomDomain()
        {
            object requestBody = DecisionTableMockClass.Temperatures.FromJson(DecisionTableMockJson.DecisionTableMock);

            dynamic result = await ManagementTest.Mt.CreateRule<object, dynamic>(requestBody);

            ManagementTest.CreatedRuleT = result.ruleId;
            ManagementTest.CreatedRuleVersionT = result.version;

            Assert.IsNotNull(result);
        }

        [Test, Order(200)]
        [TestCase(1)]
        [TestCase(0)]
        public async Task UpdateRule(int version)
        {
            object requestBody = DecisionTableMockClass.Temperatures.FromJson(DecisionTableMockJson.DecisionTableMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.UpdateRule(ManagementTest.CreatedRule, requestBody, version);
            } else
            {
                result = await ManagementTest.Mp.UpdateRule(ManagementTest.CreatedRule, requestBody);
            }

            object value = result;

            Assert.AreEqual(System.Net.HttpStatusCode.OK, value);

            Assert.IsNotNull(result);

        }

        [Test, Order(200)]
        [TestCase(1)]
        [TestCase(0)]
        public async Task UpdateRuleWithCustomDomain(int version)
        {
            object requestBody = DecisionTableMockClass.Temperatures.FromJson(DecisionTableMockJson.DecisionTableUpdateMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.UpdateRule(ManagementTest.CreatedRuleT, requestBody, version);
            }
            else
            {
                result = await ManagementTest.Mt.UpdateRule(ManagementTest.CreatedRuleT, requestBody);
            }

            object value = result;

            Debug.WriteLine(value);

            Assert.IsNotNull(result);

        }
        
        [Test, Order(300)]
        public async Task DeleteCreatedRule()
        {
            dynamic result = await ManagementTest.Mp.DeleteRule(ManagementTest.CreatedRule, ManagementTest.CreatedRuleVersion);

            object value = result;

            Assert.IsNotNull(value);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, value);
        }

        [Test, Order(300)]
        public async Task DeleteCreatedRuleWithCustomDomain()
        {
            dynamic result = await ManagementTest.Mt.DeleteRule(ManagementTest.CreatedRuleT, ManagementTest.CreatedRuleVersion);

            object value = result;

            Assert.IsNotNull(value);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, value);
        }

        [Test, Order(100)]
        public async Task CreateRuleFlow()
        {
            object request = RuleFlowMock.Temperatures.FromJson(RuleFlowMockJson.RuleFlowMock);

            dynamic result = await ManagementTest.Mp.CreateRuleFlow<object, dynamic>(request);

            ManagementTest.CreatedRuleFlow = result.compositionId;

            Assert.IsNotNull(result);

        }

        [Test, Order(100)]
        public async Task CreateRuleFlowWithCustomDomain()
        {
            object request = RuleFlowMock.Temperatures.FromJson(RuleFlowMockJson.RuleFlowMock);

            dynamic result = await ManagementTest.Mp.CreateRuleFlow<object, dynamic>(request);

            ManagementTest.CreatedRuleFlow = result.compositionId;

            ManagementTest.CreatedRuleFlowT = result.compositionId;

            Assert.IsNotNull(result);
        }

        [Test, Order(200)]
        [TestCase(1)]
        [TestCase(0)]
        public async Task UpdateRuleFlow(int version)
        {
            object requestBody = RuleFlowMock.Temperatures.FromJson(RuleFlowMockJson.RuleFlowUpdateMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.UpdateRuleFlow(ManagementTest.CreatedRuleFlow, requestBody, version);
            }
            else
            {
                result = await ManagementTest.Mt.UpdateRuleFlow(ManagementTest.CreatedRuleFlow, requestBody);
            }

            object value = result;

            Assert.IsNotNull(result);

        }

        [Test, Order(200)]
        [TestCase(1)]
        [TestCase(0)]
        public async Task UpdateRuleFlowWithCustomDomain(int version)
        {
            object requestBody = RuleFlowMock.Temperatures.FromJson(RuleFlowMockJson.RuleFlowUpdateMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.UpdateRuleFlow(ManagementTest.CreatedRuleFlowT, requestBody, version);
            }
            else
            {
                result = await ManagementTest.Mt.UpdateRuleFlow(ManagementTest.CreatedRuleFlowT, requestBody);
            }

            object value = result;

            Assert.IsNotNull(result);

        }

        [Test, Order(300)]
        public async Task DeleteCreatedRuleFlow()
        {
            dynamic result = await ManagementTest.Mp.DeleteRule(ManagementTest.CreatedRule, ManagementTest.CreatedRuleVersion);

            object value = result;

            Assert.IsNotNull(value);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, value);
        }

        [Test, Order(300)]
        public async Task DeleteCreatedRuleFlowWithCustomDomain()
        {
            dynamic result = await ManagementTest.Mt.DeleteRule(ManagementTest.CreatedRuleT, ManagementTest.CreatedRuleVersion);

            object value = result;

            Assert.IsNotNull(value);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, value);
        }

        [Test, Order(100)]
        [TestCase(0, "44308135-ca0d-6519-a36a-c20f844925d4")]
        [TestCase(1, "44308135-ca0d-6519-a36a-c20f844925d4")]
        public async Task AddTagsToRule(int version, string ruleId)
        {
            object request = TagMock.Temperatures.FromJson(TagMock.TagMockJson.TagMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.UpdateTags<object, dynamic>(request, ruleId, version);
            } else
            {
                result = await ManagementTest.Mp.UpdateTags<object, dynamic>(request, ruleId);
            }

            object value = result.message;

            Assert.IsNotNull(result);
            Assert.AreEqual("ok", value.ToString());
        }

        [Test, Order(100)]
        [TestCase(0, "f2b91506-d607-cd45-8df4-f29f5dacaa84")]
        [TestCase(1, "f2b91506-d607-cd45-8df4-f29f5dacaa84")]
        public async Task AddTagsToRuleWithCustomDomain(int version, string ruleId)
        {
            object request = TagMock.Temperatures.FromJson(TagMock.TagMockJson.TagMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.UpdateTags<object, dynamic>(request, ruleId, version);
            }
            else
            {
                result = await ManagementTest.Mt.UpdateTags<object, dynamic>(request, ruleId);
            }

            object value = result.message;

            Assert.IsNotNull(result);
            Assert.AreEqual("ok", value.ToString());
        }

        [Test, Order(200)]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", new string[] {"testName"})]
        [TestCase("44308135-ca0d-6519-a36a-c20f844925d4", new string[] {"testName"})]
        public async Task GetTags(string ruleId, string[] tags)
        {

            dynamic result = await ManagementTest.Mp.GetSpaceItems<dynamic>(tags);

            string ruleIdR = result[0].ruleId;
            string tag = result[0].tags[0];

            Assert.IsNotNull(result);
            Assert.AreEqual(tags[0], tag);
            Assert.AreEqual(ruleId, ruleIdR);
        }

        [Test, Order(200)]
        [TestCase("f2b91506-d607-cd45-8df4-f29f5dacaa84", new string[] { "testName" })]
        [TestCase("f2b91506-d607-cd45-8df4-f29f5dacaa84", new string[] { "testName" })]
        public async Task GetTagsWithCustomDomain(string ruleId, string[] tags)
        {

            dynamic result = await ManagementTest.Mt.GetSpaceItems<dynamic>(tags);

            string ruleIdR = result[0].ruleId;
            string tag = result[0].tags[0];

            Assert.IsNotNull(result);
            Assert.AreEqual(tags[0], tag);
            Assert.AreEqual(ruleId, ruleIdR);
        }

        [Test, Order(300)]
        [TestCase(0, "44308135-ca0d-6519-a36a-c20f844925d4", new string[] { "testName" })]
        [TestCase(1, "44308135-ca0d-6519-a36a-c20f844925d4", new string[] { "testName" })]
        public async Task DeleteTags(int version, string ruleId, string[] tags)
        {
            object request = TagMock.Temperatures.FromJson(TagMock.TagMockJson.TagMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mp.DeleteTags(ruleId, tags, version);
            }
            else
            {
                result = await ManagementTest.Mp.DeleteTags(ruleId, tags);
            }

            object value = result;

            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, result);
        }

        [Test, Order(300)]
        [TestCase(0, "f2b91506-d607-cd45-8df4-f29f5dacaa84", new string[] { "testName" })]
        [TestCase(1, "f2b91506-d607-cd45-8df4-f29f5dacaa84", new string[] { "testName" })]
        public async Task DeleteTagsWithCustomDomain(int version, string ruleId, string[] tags)
        {
            object request = TagMock.Temperatures.FromJson(TagMock.TagMockJson.TagMock);

            dynamic result;

            if (version > 0)
            {
                result = await ManagementTest.Mt.DeleteTags(ruleId, tags, version);
            }
            else
            {
                result = await ManagementTest.Mt.DeleteTags(ruleId, tags);
            }

            object value = result;

            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, result);
        }
    }
}
