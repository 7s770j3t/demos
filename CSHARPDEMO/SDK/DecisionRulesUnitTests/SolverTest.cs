using DecisionRules;
using DecisionRules.Exceptions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace DecisionRulesUnitTests
{

    class RequestSample
    {
        public string? Period { get; set; }
        public string? ProductType { get; set; }
        public string? PromoCode { get; set; }
    }

    

    [TestFixture]
    public class SolverTest
    {
        private static readonly string ApiKeyP = "SMuRzbhtU7slOvHDoNNTeTpQHW317CYQI8CTJXapsEmD-gqtn8FJzJpUdPhVN8eX";
        private static readonly string ApiKeyT = "RCq5g4iOZHGysNu7Vz1wSdijbXES8FXbxKJpCZgaV1Y6G9_xTPp9fu4ckdrNYl_J";

        private static CustomDomain domain = new CustomDomain("test.api.decisionrules.io", Enums.Protocol.HTTPS, 443);

        [Test]
        [TestCase("f48ac9a7-25cd-1168-05e8-23445573655c")]
        public async Task ShouldSolveRuleWithoutCustomDomain(string ruleId)
        {
            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyP);
            List<dynamic> response = await solver.SolveRule<RequestSample, dynamic>(ruleId, request);

            float finalPrice = response[0].prices.finalPrice;
            int crudePrice = response[0].prices.crudePrice;
            string message = response[0].message;


            Assert.AreEqual("5.6", finalPrice.ToString());
            Assert.AreEqual(crudePrice, 8);
            Assert.AreEqual(message, "30% discount");

            Assert.IsNotNull(response);
        }

        [Test]
        [TestCase("bdb22823-fe84-aca1-da38-6f4b9f8c106a")]
        public async Task ShouldSolveRuleWithtCustomDomain(string ruleId)
        {

            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyT, SolverTest.domain);
            List<dynamic> response = await solver.SolveRule<RequestSample, object>(ruleId, request);

            float finalPrice = response[0].prices.finalPrice;
            int crudePrice = response[0].prices.crudePrice;
            string message = response[0].message;


            Assert.AreEqual("5.6", finalPrice.ToString());
            Assert.AreEqual(crudePrice, 8);
            Assert.AreEqual(message, "30% discount");

            Assert.IsNotNull(response, response[0].ToString());
        }

        [Test]
        [TestCase("df0b2225-e34d-a708-5054-1ff81d51b753")]
        public async Task ShouldSolveRuleFlowWithoutCustomDomain(string ruleId)
        {
            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyP);
            List<dynamic> response = await solver.SolveRuleFlow<RequestSample, dynamic>(ruleId, request);

            float finalPrice = response[0].finalPrice;
            int crudePrice = response[0].crudePrice;
            string message = response[0].message;

            Assert.AreEqual("5.6", finalPrice.ToString());
            Assert.AreEqual(crudePrice, 8);
            Assert.AreEqual(message, "30% discount");

            Assert.IsNotNull(response);
        }

        [Test]
        [TestCase("a149dd0b-126d-6c7e-0f99-c7198cab6240")]
        public async Task ShouldSolveRuleFlowWithtCustomDomain(string ruleId)
        {
            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyT, SolverTest.domain);
            List<dynamic> response = await solver.SolveRuleFlow<RequestSample, dynamic>(ruleId, request);

            float finalPrice = response[0].finalPrice;
            int crudePrice = response[0].crudePrice;
            string message = response[0].message;

            Assert.AreEqual("5.6", finalPrice.ToString());
            Assert.AreEqual(crudePrice, 8);
            Assert.AreEqual(message, "30% discount");

            Assert.IsNotNull(response);
        }

        [Test]
        [TestCase("fake")]
        public void ShouldReturnErrorWithInvalidRuleId(string ruleId)
        {
            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyT, SolverTest.domain);

            Assert.ThrowsAsync<FailedToSolveRuleException>(() => solver.SolveRule<RequestSample, dynamic>(ruleId, request));
            
        }

        [Test]
        [TestCase("fake")]
        public void ShouldReturnErrorWithInvalidRuleFlowId(string ruleId)
        {
            RequestSample request = new RequestSample { Period = "month", ProductType = "basic", PromoCode = "SUMMER SALE" };

            Solver solver = new(SolverTest.ApiKeyT, SolverTest.domain);

            Assert.ThrowsAsync<FailedToSolveRuleException>(() => solver.SolveRule<RequestSample, dynamic>(ruleId, request));
        }
    }
}