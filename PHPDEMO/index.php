<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lib</title>
</head>
<body>
   <?php 
        include 'decisionrules.php';
        include 'dr_management.php';
        require_once 'dataForTest.php';

        $customDomain = new CustomDomain("test.api.decisionrules.io", Protocols::HTTPS, 443);

        $decisionRules = new DecisionRules('h7t6JrNg-7DO9xdj2IDjGs4YekaIlYbs99f_3Q4lwYYl2f6U4hg-jxukoRmdsRVI', $customDomain);

        $managementAPI = new DrManagementApi("Rl4wQTlgoGvvflinZpFEehPyZ1Udnq7OWOkdsDtIIGU2uBQaarAfhYACbOnj1XlA", $customDomain);

        $data = array (
            'data' => 
            array (
              'say' => 'today',
            ),
        );

        $response = $decisionRules->Solver(SolverTypes::RULE ,"0eb0fea6-bf12-fc42-6954-2e681e1ee557", $data, SolverStrategy::STANDARD, 1);

        $response_composition = $decisionRules->Solver(SolverTypes::COMPOSITION, "28f8e4e0-1862-0cbd-7e2f-b5a9f237084e", $data, SolverStrategy::STANDARD);

        print_r($response);

        print_r($response_composition);

        $getRuleId = $managementAPI->getRuleById("0eb0fea6-bf12-fc42-6954-2e681e1ee557");
        $getRuleAndVersion = $managementAPI->getRuleByIdAndVersion("0eb0fea6-bf12-fc42-6954-2e681e1ee557", "1");
        $getSpace = $managementAPI->getSpaceInfo("2cbcd42e-bb0e-7a42-8187-029ec401bd75");

        $postRule = $managementAPI->postRuleForSpace("2cbcd42e-bb0e-7a42-8187-029ec401bd75", $postDATA);
        $putRule = $managementAPI->putRule("680a08da-729a-bc80-7e8a-9814edd75769", "1", $putDATA);

        $deleteRule = $managementAPI->deleteRule("61c5fc30-0dcc-68ee-5472-e744eb4a3482", "1");

        print_r($getRuleId);
        print_r($getRuleAndVersion);
        print_r($getSpace);

   ?>
</body>
</html>