<?php

    abstract class GeoLocation{
        const DEFAULT = NULL;
        const EU1 = 'eu1';
        const EU2 = 'eu2';
        const US1 = 'us1';
        const US2 = 'us2';
    }