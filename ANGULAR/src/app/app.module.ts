import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgDecisionrulesModule} from '@decisionrules/ng-decisionrules';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgDecisionrulesModule.forRoot({
      auth: {
        token: "rkuKs2r9N9ZUPba-_gh2UXcEOFeIu9-LfcotXrfM1lytRbgU7QYmd0GvqUtXqM7f",
        managementToken: "JHptoC1GFWd-pvFIAR49pgAS7dobTjwZUM-178VcqHEppGJsnbmoC1-vYt0lJn1c"
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
