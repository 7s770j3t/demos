import {Component, OnInit} from '@angular/core';
import {
  NgDecisionrulesPublicService,
  NgDecisionrulesService,
  SolverStrategyEnum,
  SolverTypeEnum
} from '@decisionrules/ng-decisionrules';
import {rule} from './rule';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private drs: NgDecisionrulesService,
              private drm: NgDecisionrulesPublicService) {
  }

  private inputData = {
    say: "Hello from angular"
  }

  private ruleId = "7fa62496-d5b3-df8d-bfd0-871de2a7732d";
  private delete = "cc3c0254-01fc-23e0-c77f-59a66727fd98";
  private space = "b53c8c35-98c4-2bb9-a719-97dbdc97424b";
  private update = "64281529-58db-9c80-e1da-fcaa7da5a746";

  resolved: any[] = [];

  private display: string[]= [];

  async ngOnInit() {
    const strategy = SolverStrategyEnum.STANDARD;
    const type = SolverTypeEnum.RULE;
    const test = await this.drs.solveRule(this.inputData, this.ruleId, SolverStrategyEnum.STANDARD, SolverTypeEnum.RULE);

    console.log(test);

    const results = [];
    results.push(this.drm.getRuleById(this.ruleId));
    results.push(this.drm.getRuleByIdAndVersion(this.ruleId, '1'));
    results.push(this.drm.deleteRuleByRuleIdAndVersion(this.delete, '1'));
    results.push(this.drm.getRulesForSpace(this.space));
    results.push(this.drm.createRuleForSpace(this.space, rule));
    results.push(this.drm.updateRuleByIdAndVersion(this.update, 1, rule));

    console.log(await Promise.all(results));

  }
}
