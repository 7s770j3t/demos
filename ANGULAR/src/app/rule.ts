export const rule = {
  "name": "POST ANGULAR",
  "description": "",
  "inputSchema": {
    "say": {}
  },
  "outputSchema": {
    "Output Attribute": {}
  },
  "decisionTable": {
    "columns": [
      {
        "condition": {
          "type": "simple",
          "inputVariable": "say",
          "name": "New Condition"
        },
        "columnId": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",
        "type": "input"
      },
      {
        "columnOutput": {
          "type": "simple",
          "outputVariable": "Output Attribute",
          "name": "New Result"
        },
        "columnId": "2e46eb73-de05-51bc-5913-4b261bbe2069",
        "type": "output"
      }
    ],
    "rows": [
      {
        "cells": [
          {
            "column": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",
            "scalarCondition": {
              "value": "",
              "operator": "anything"
            },
            "type": "input"
          },
          {
            "column": "2e46eb73-de05-51bc-5913-4b261bbe2069",
            "outputScalarValue": {
              "type": "function",
              "value": "{say}"
            },
            "type": "output"
          }
        ],
        "active": true
      }
    ]
  },
  "type": "decision-table",
  "status": "published",
  "createdIn": "2021-10-25T15:56:04.764Z",
  "lastUpdate": "2021-10-25T15:56:34.352Z"
}
