import {DecisionRulesConfigModel} from '@decisionrules/decisionrules-js/dist/models/Models';
import {GeoLocation} from '@decisionrules/decisionrules-js/dist/enums/GeoLocations';
import {Solver} from '@decisionrules/decisionrules-js/dist/Solver';
import {SolverStrategy} from '@decisionrules/decisionrules-js/dist/enums/SolverStrategies';
import {SolverTypes} from '@decisionrules/decisionrules-js/dist/enums/SolverTypes';
import {CustomDomain} from '@decisionrules/decisionrules-js/dist/CustomDomain';
import {Protocols} from '@decisionrules/decisionrules-js/dist/enums/Protocols';
import {DrManagementApi} from '@decisionrules/decisionrules-js/dist/DrManagementApi';

export class Main {

    private decisionrules;
    private publicApi;
    private readonly config: DecisionRulesConfigModel;

    constructor() {
        this.config = {
            authKey : "h7t6JrNg-7DO9xdj2IDjGs4YekaIlYbs99f_3Q4lwYYl2f6U4hg-jxukoRmdsRVI",
            geoLoc : GeoLocation.DEFAULT,
            strategy : SolverStrategy.STANDARD,
            customDomain: new CustomDomain(Protocols.HTTP, "api.decisionrules.io"),
            publicAuthKey: "Rl4wQTlgoGvvflinZpFEehPyZ1Udnq7OWOkdsDtIIGU2uBQaarAfhYACbOnj1XlA"
        }
        this.decisionrules = new Solver(this.config);
        this.publicApi = new DrManagementApi(this.config);
    }

    private data = {
        data: {
            say: "Do not eat hedgehogs"
        }
    }

    private publicData = JSON.parse('{\n' +
        '    "name": "POSTRULE",\n' +
        '    "description": "",\n' +
        '    "inputSchema": {\n' +
        '        "Input attribute": {}\n' +
        '    },\n' +
        '    "outputSchema": {\n' +
        '        "Output Attribute": {}\n' +
        '    },\n' +
        '    "decisionTable": {\n' +
        '        "columns": [\n' +
        '            {\n' +
        '                "condition": {\n' +
        '                    "type": "simple",\n' +
        '                    "inputVariable": "Input attribute",\n' +
        '                    "name": "New Condition"\n' +
        '                },\n' +
        '                "columnId": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",\n' +
        '                "type": "input"\n' +
        '            },\n' +
        '            {\n' +
        '                "columnOutput": {\n' +
        '                    "type": "simple",\n' +
        '                    "outputVariable": "Output Attribute",\n' +
        '                    "name": "New Result"\n' +
        '                },\n' +
        '                "columnId": "2e46eb73-de05-51bc-5913-4b261bbe2069",\n' +
        '                "type": "output"\n' +
        '            }\n' +
        '        ],\n' +
        '        "rows": [\n' +
        '            {\n' +
        '                "cells": [\n' +
        '                    {\n' +
        '                        "column": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",\n' +
        '                        "scalarCondition": {\n' +
        '                            "value": "",\n' +
        '                            "operator": "anything"\n' +
        '                        },\n' +
        '                        "type": "input"\n' +
        '                    },\n' +
        '                    {\n' +
        '                        "column": "2e46eb73-de05-51bc-5913-4b261bbe2069",\n' +
        '                        "outputScalarValue": {\n' +
        '                            "type": "common",\n' +
        '                            "value": "Hello from Solver"\n' +
        '                        },\n' +
        '                        "type": "output"\n' +
        '                    }\n' +
        '                ]\n' +
        '            }\n' +
        '        ]\n' +
        '    },\n' +
        '    "type": "decision-table",\n' +
        '    "status": "published",\n' +
        '    "createdIn": "2021-09-23T08:14:07.852Z",\n' +
        '    "lastUpdate": "2021-09-23T08:14:07.852Z"\n' +
        '}');

    private putData = JSON.parse('{\n' +
        '    "name": "I PUT IT HERE",\n' +
        '    "description": "",\n' +
        '    "inputSchema": {\n' +
        '        "Input attribute": {}\n' +
        '    },\n' +
        '    "outputSchema": {\n' +
        '        "Output Attribute": {}\n' +
        '    },\n' +
        '    "decisionTable": {\n' +
        '        "columns": [\n' +
        '            {\n' +
        '                "condition": {\n' +
        '                    "type": "simple",\n' +
        '                    "inputVariable": "Input attribute",\n' +
        '                    "name": "New Condition"\n' +
        '                },\n' +
        '                "columnId": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",\n' +
        '                "type": "input"\n' +
        '            },\n' +
        '            {\n' +
        '                "columnOutput": {\n' +
        '                    "type": "simple",\n' +
        '                    "outputVariable": "Output Attribute",\n' +
        '                    "name": "New Result"\n' +
        '                },\n' +
        '                "columnId": "2e46eb73-de05-51bc-5913-4b261bbe2069",\n' +
        '                "type": "output"\n' +
        '            }\n' +
        '        ],\n' +
        '        "rows": [\n' +
        '            {\n' +
        '                "cells": [\n' +
        '                    {\n' +
        '                        "column": "ec57bb7c-8e90-4aee-da49-17b607a6b09a",\n' +
        '                        "scalarCondition": {\n' +
        '                            "value": "",\n' +
        '                            "operator": "anything"\n' +
        '                        },\n' +
        '                        "type": "input"\n' +
        '                    },\n' +
        '                    {\n' +
        '                        "column": "2e46eb73-de05-51bc-5913-4b261bbe2069",\n' +
        '                        "outputScalarValue": {\n' +
        '                            "type": "common",\n' +
        '                            "value": "Hello from Solver"\n' +
        '                        },\n' +
        '                        "type": "output"\n' +
        '                    }\n' +
        '                ]\n' +
        '            }\n' +
        '        ]\n' +
        '    },\n' +
        '    "type": "decision-table",\n' +
        '    "status": "published",\n' +
        '    "createdIn": "2021-09-23T08:14:07.852Z",\n' +
        '    "lastUpdate": "2021-09-23T08:14:07.852Z"\n' +
        '}');

    private readonly ruleId = "0eb0fea6-bf12-fc42-6954-2e681e1ee557";
    private readonly spaceId = "2cbcd42e-bb0e-7a42-8187-029ec401bd75"

    async call(){
        return await this.decisionrules.solver(SolverTypes.RULE, this.data, this.ruleId)
    }

    async getRuleById(){
        return await this.publicApi.getRuleById(this.ruleId);
    }

    async getRuleByIdAndVersion(){
        return await this.publicApi.getRuleByIdAndVersion(this.ruleId, "1");
    }

    async getSpaceInfo(){
        return await this.publicApi.getSpace(this.spaceId);
    }

    async postRule(){
        return await this.publicApi.postRuleForSpace(this.spaceId, this.publicData);
    }

    async putRule(){
        return await this.publicApi.putRule("939c0f87-e510-a309-426d-cce53edf41e1", "1", this.putData);
    }

    async deleteRule(){
        return await this.publicApi.deleteRule("b6ae0635-70a6-0412-78a4-268d9bdbadfe", "1");
    }
}

const testCall = new Main();

function solverCall(){
    testCall.call().then(r => {
        console.log('SOLVER: ',r);
    });
    testCall.getRuleById().then(r => {
       console.log('GET RULE BY ID: ',r);
    });
    testCall.getRuleByIdAndVersion().then(r => {
        console.log('GET RULE BY ID: ',r);
    });
    testCall.getSpaceInfo().then(r => {
        console.log('GET SPACE BY ID: ',r);
    });
    testCall.postRule().then(r => {
       console.log('POST RULE: ',r);
    });
    testCall.putRule().then(r => {
        console.log('PUT RULE: ',r);
    });
    testCall.deleteRule().then(r => {
       console.log('DELETE RULE: ',r);
    });
}

solverCall();
