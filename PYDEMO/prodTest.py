import decisionrules
from postput_data import put_data, post_data, new_rf
import json
import asyncio

SOLVER_API_KEY = "rkuKs2r9N9ZUPba-_gh2UXcEOFeIu9-LfcotXrfM1lytRbgU7QYmd0GvqUtXqM7f"
SOLVER_RULE_ID = "a56aceae-005b-b29e-0b8c-b94b94df1fd6"
SOLVER_RULEFLOW = "7af04cdb-f09c-5185-fe60-88e75aa16940"

SPACE_ID = "b53c8c35-98c4-2bb9-a719-97dbdc97424b"

MANAGEMENT_KEY = "JHptoC1GFWd-pvFIAR49pgAS7dobTjwZUM-178VcqHEppGJsnbmoC1-vYt0lJn1c"

async def prod_solver():
    data = {"in": "12"}

    solver = decisionrules.SolverApi(SOLVER_API_KEY)
    response = await solver.solve(decisionrules.SolverType.RULE, SOLVER_RULE_ID, data, decisionrules.SolverStrategies.STANDARD)

    print("RULE OUTPUT: ", response)

    response2 = await solver.solve(decisionrules.SolverType.RULEFLOW, SOLVER_RULEFLOW, data,
                                           decisionrules.SolverStrategies.STANDARD)

    print("COMPOSITION OUTPUT: ", response2)


async def prod_crud():
    management_api_instance_classic = decisionrules.ManagementApi(MANAGEMENT_KEY)


    resposne = await management_api_instance_classic.create_rule(SPACE_ID, post_data)
    ruleId = json.loads(json.dumps(resposne))
    id = ruleId["ruleId"]
    await management_api_instance_classic.update_rule(id, "1", put_data)

    get_rule = await management_api_instance_classic.get_rule(id)
    get_rule_v = await management_api_instance_classic.get_rule(id, "1")
    get_space = await management_api_instance_classic.get_space(SPACE_ID)

    print(get_rule)
    print(get_rule_v)
    print(get_space)

    await management_api_instance_classic.delete_rule(id, "1")


async def prod_rf_crud():
    custom = decisionrules.CustomDomain("api.decisionrules.io", decisionrules.Protocols.HTTPS)
    management_api_instance = decisionrules.ManagementApi(MANAGEMENT_KEY, custom)

    create = await management_api_instance.create_ruleflow(new_rf)
    ruleflow_id = json.loads(json.dumps(create))
    id = ruleflow_id["compositionId"]
    get1 = await management_api_instance.get_ruleflow(id)
    print(get1)
 
    get2 = await management_api_instance.get_ruleflow(id, 1)
    print(get2)
    await management_api_instance.update_ruleflow(id, 1, create)
    export = await management_api_instance.export_ruleflow(id)
    export1 = await management_api_instance.export_ruleflow(id, 1)
    print(export)
    print(export1)
    importR = await management_api_instance.import_ruleflow([new_rf])
    importR2 = await management_api_instance.import_ruleflow([new_rf], id)
    importR3 = await management_api_instance.import_ruleflow([new_rf], id, 1)
    print(importR)
    print(importR2)
    print(importR3)

def main():
    asyncio.run(prod_solver())
    asyncio.run(prod_crud())
    asyncio.run(prod_rf_crud())



if __name__ == "__main__":
    main()