import asyncio
import decisionrules
from postput_data import put_data, post_data, new_rf
import json


api_key = "h7t6JrNg-7DO9xdj2IDjGs4YekaIlYbs99f_3Q4lwYYl2f6U4hg-jxukoRmdsRVI"
mng_key = "Rl4wQTlgoGvvflinZpFEehPyZ1Udnq7OWOkdsDtIIGU2uBQaarAfhYACbOnj1XlA"
mng_key2 = "WuSd5P3QLhLdLQi_uV2P0qiRjiDxugISs-OtiuUt6hTFGrJ9a7ZnWITvRtY4x_Mz"
space_key = "361602af-8dc0-e954-7c2d-9bdb31aba4cd"

get_rule = "0eb0fea6-bf12-fc42-6954-2e681e1ee557"
get_space = "2cbcd42e-bb0e-7a42-8187-029ec401bd75"

post_rule = "2cbcd42e-bb0e-7a42-8187-029ec401bd75"
put_rule = "096f17ca-e103-1e23-7cd9-db074392c82e"
delete_rule = "5b1052f2-7c31-4075-61b1-f99ea1fd7537"

compo_rule = "28f8e4e0-1862-0cbd-7e2f-b5a9f237084e"



async def solver_test():
    data = {"say": "Hello from python"}

    solver = decisionrules.SolverApi(api_key)
    response = await solver.solve(decisionrules.SolverType.RULE, get_rule, data, decisionrules.SolverStrategies.STANDARD)

    print("RULE OUTPUT: ", response)

    response2 = await solver.solve(decisionrules.SolverType.RULEFLOW, compo_rule, data,
                                           decisionrules.SolverStrategies.STANDARD)

    print("COMPOSITION OUTPUT: ", response2)


async def crud_test():
    management_api_instance_classic = decisionrules.ManagementApi(mng_key)

    get_rule_resp = await management_api_instance_classic.get_rule(get_rule)
    get_rule_by_version_resp = await management_api_instance_classic.get_rule(get_rule, "1")
    get_space_resp = await management_api_instance_classic.get_space(get_space)

    await management_api_instance_classic.update_rule(put_rule, "1", put_data)
    await management_api_instance_classic.create_rule(post_rule, post_data)
    await management_api_instance_classic.delete_rule(delete_rule, "1")

    print("GET_RULE: ", get_rule_resp)
    print("GET_RULE VER: ", get_rule_by_version_resp)
    print("GET_SPACE: ", get_space_resp)


async def rf_management_demo():
    custom = decisionrules.CustomDomain("test.api.decisionrules.io", decisionrules.Protocols.HTTPS)
    management_api_instance = decisionrules.ManagementApi(mng_key2, custom)

    create = await management_api_instance.create_ruleflow(new_rf)
    ruleflow_id = json.loads(json.dumps(create))
    id = ruleflow_id["compositionId"]
    get1 = await management_api_instance.get_ruleflow(id)
    print(get1)
 
    get2 = await management_api_instance.get_ruleflow(id, 1)
    print(get2)
    await management_api_instance.update_ruleflow(id, 1, create)
    export = await management_api_instance.export_ruleflow(id)
    export1 = await management_api_instance.export_ruleflow(id, 1)
    print(export)
    print(export1)
    importR = await management_api_instance.import_ruleflow([new_rf])
    importR2 = await management_api_instance.import_ruleflow([new_rf], id)
    importR3 = await management_api_instance.import_ruleflow([new_rf], id, 1)
    print(importR)
    print(importR2)
    print(importR3)



def main():
    asyncio.run(rf_management_demo())
    asyncio.run(crud_test())
    asyncio.run(solver_test())


if __name__ == "__main__":
    main()
