put_data = {
    "name": "PUT FROM PYTHON TEST",
    "description": "Sample client profitability rule",
    "inputSchema": {
        "say": {}
    },
    "outputSchema": {
        "parrot": {}
    },
    "decisionTable": {
        "columns": [
            {
                "condition": {
                    "type": "simple",
                    "inputVariable": "say",
                    "name": "Investments"
                },
                "columnId": "59ad9cc8-4410-54ad-0229-75f30df26f73",
                "type": "input"
            },
            {
                "columnOutput": {
                    "type": "simple",
                    "outputVariable": "parrot",
                    "name": "Profitability Ratio"
                },
                "columnId": "59d3aa91-fe3e-0a39-20de-55c06b888885",
                "type": "output"
            }
        ],
        "rows": [
            {
                "cells": [
                    {
                        "column": "59ad9cc8-4410-54ad-0229-75f30df26f73",
                        "scalarCondition": {
                            "value": "",
                            "operator": "anything",
                            "type": "general"
                        },
                        "type": "input"
                    },
                    {
                        "column": "59d3aa91-fe3e-0a39-20de-55c06b888885",
                        "outputScalarValue": {
                            "type": "function",
                            "value": "{say}"
                        },
                        "type": "output"
                    }
                ],
                "active": True
            }
        ]
    },
    "type": "decision-table",
    "status": "published",
    "createdIn": "2021-09-24T09:12:36.161Z",
    "lastUpdate": "2021-09-24T09:15:16.341Z"
}

post_data = {
    "name": "POSTED FROM PYTHON TEST",
    "description": "Sample client profitability rule",
    "inputSchema": {
        "say": {}
    },
    "outputSchema": {
        "parrot": {}
    },
    "decisionTable": {
        "columns": [
            {
                "condition": {
                    "type": "simple",
                    "inputVariable": "say",
                    "name": "Investments"
                },
                "columnId": "59ad9cc8-4410-54ad-0229-75f30df26f73",
                "type": "input"
            },
            {
                "columnOutput": {
                    "type": "simple",
                    "outputVariable": "parrot",
                    "name": "Profitability Ratio"
                },
                "columnId": "59d3aa91-fe3e-0a39-20de-55c06b888885",
                "type": "output"
            }
        ],
        "rows": [
            {
                "cells": [
                    {
                        "column": "59ad9cc8-4410-54ad-0229-75f30df26f73",
                        "scalarCondition": {
                            "value": "",
                            "operator": "anything",
                            "type": "general"
                        },
                        "type": "input"
                    },
                    {
                        "column": "59d3aa91-fe3e-0a39-20de-55c06b888885",
                        "outputScalarValue": {
                            "type": "function",
                            "value": "{say}"
                        },
                        "type": "output"
                    }
                ],
                "active": True
            }
        ]
    },
    "type": "decision-table",
    "status": "published",
    "createdIn": "2021-09-24T09:12:36.161Z",
    "lastUpdate": "2021-09-24T09:15:16.341Z"
}


new_rf = {
    "name": "trest",
    "inputSchema": {},
    "outputSchema": {},
    "type": "composition",
    "status": "pending",
    "dataTree": {},
    "visualEditorData": {}
}